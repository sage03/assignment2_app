class PeopleController < ApplicationController
  def index
     @people = Person.all
  end

  # GET /people/1
  # GET /people/1.json
  def show
   @people = Person.all
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)
    if @person.save 
      redirect_to(:action =>'index')
    else 
      render('new')
    end
  end
  
    #respond_to do |format|
     # if @person.save
      #  format.html { redirect_to @person, notice: 'Person was successfully created.' }
       # format.json { render :show, status: :created, location: @person }
      #else
       # format.html { render :new }
        #format.json { render json: @person.errors, status: :unprocessable_entity }
      #end
    #end
  #end
#
  def edit
    @person = Person.find(params[:id])
  end
  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    @person = Person.find(params[:id])
    @person.update_attributes(person_params)
    redirect_to({:action=> 'index'})
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def delete
     @person = Person.find(params[:id])
  end  
  def destroy
    @person = Person.find(params[:id])
    @person.destroy
    redirect_to({:action=> 'index'})
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:name, :weight, :height, :color, :sex)
    end
end
